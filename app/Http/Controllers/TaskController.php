<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tasks = Task::all();

        return view('tasks.index')->with('tasks', $tasks);
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function store(Request $request)
    {
        $task =Task::find($id);

        $task->title = $request->title;
        $task->deadline = $request->deadline;
        $task->description = $request->description;
       

        $task->save();

        return redirect()->route('tareas.index');
    }

    public function show( $id)
    {
        //
    }

    public function edit( $id)
    {
        
        $taks= Task::find($id);
        return view('tasks.edit');

    }

    public function update(Request $request,  $id)
    {
        $task = new Task;

        $task->title = $request->title;
        $task->deadline = $request->deadline;
        $task->description = $request->description;
       

        $task->save();
    }

    public function destroy( $id)
    {
        //
    }
}